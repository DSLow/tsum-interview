package tsum.common;

import org.apache.commons.lang3.RandomStringUtils;


/**
 * @project testProject
 * Created by Andrey M on 02.01.2021.
 */
public class CommonFields {

	public static final String testEmail = "brilee25@utclubsxu.com";
	public static final String testPassword = "12345678";

	public static String generatedRandom(Integer number) {
		return RandomStringUtils.random(number, "abcdefghijklmnopqrstuvwxyz0123456789");
	}

	public static String generatedRandomEmail(Integer number) {
		return RandomStringUtils.randomAlphabetic(number) + "@test.test";
	}
}
