package tsum;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import tsum.steps.AuthorizationSteps;
import tsum.steps.RegistrationSteps;

import static tsum.common.CommonFields.*;


/**
 * @project testProject
 * Created by Andrey M on 01.01.2021.
 */
@RunWith(SerenityRunner.class)
public class TestClass {

	@Managed(driver = "chrome")
	WebDriver driver;

	@Steps
	RegistrationSteps registration;

	@Steps
	AuthorizationSteps authorization;


	@Test
	@WithTags( {
			@WithTag(type="type",name="positive"),
			@WithTag(type="component",name="registration")

	})
	public void positiveRegistrationTest() {
		registration.openRegistrationPage();
		registration.fillRegistrationFields(generatedRandomEmail(8),
		                                    generatedRandom(8));
		registration.confirmRegistration();
		registration.shouldSeeASuccessfulNotification("Успешная регистрация");
	}

	@Test
	@WithTags( {
			@WithTag(type="type",name="negative"),
			@WithTag(type="component",name="registration")

	})
	public void negativeRegistrationTest() {
		registration.openRegistrationPage();
		registration.fillRegistrationFields(generatedRandomEmail(8),
		                                    generatedRandom(7));
		registration.confirmRegistration();
		registration.shouldSeeAnErrorNotification("Пароль должен быть не менее 8 символов длиной");
	}

	@Test
	@WithTags( {
			@WithTag(type="type",name="positive"),
			@WithTag(type="component",name="authorization")

	})
	public void positiveAuthorizationTest() {
		authorization.openAuthorizationPage();
		authorization.fillAuthorizationFields(testEmail, testPassword);
		authorization.confirmAuthorization();
		authorization.shouldSeeSelfOnTheMenu(testEmail);
	}

	@Test
	@WithTags( {
		@WithTag(type="type",name="negative"),
		@WithTag(type="component",name="authorization")

	})
	public void negativeAuthorizationTest() {
		authorization.openAuthorizationPage();
		authorization.fillAuthorizationFields(generatedRandomEmail(8),
		                                      generatedRandom(8));
		authorization.confirmAuthorization();
		authorization.shouldSeeAnErrorNotification("Неверный логин или пароль");
	}

	@After
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}
}