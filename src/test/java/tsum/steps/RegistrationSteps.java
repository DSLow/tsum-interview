package tsum.steps;

import net.thucydides.core.annotations.Step;
import tsum.pages.HomePage;
import tsum.pages.RegistrationPage;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @project testProject
 * Created by Andrey M on 01.01.2021.
 */
public class RegistrationSteps {

    RegistrationPage registration;
    HomePage home;

    @Step()
    public void openRegistrationPage(){
    	registration.open();
    }

	@Step
	public void fillRegistrationFields(String email, String password) {
    	registration.inputEmailField(email);
    	registration.inputPasswordField(password);
	}

	@Step
	public void confirmRegistration() {
    	registration.submitButtonClick();
	}

	@Step
	public void shouldSeeASuccessfulNotification(String text) {
    	home.successNotificationIsPresented();
    	home.successNotificationContainsText(text);
		assertThat(registration.submitRegistrationButtonIsVisible()).isFalse();
	}

	@Step
	public void shouldSeeAnErrorNotification(String text) {
		home.errorNotificationIsPresented();
		home.errorNotificationContainsText(text);
	}
}