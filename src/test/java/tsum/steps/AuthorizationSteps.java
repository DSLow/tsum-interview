package tsum.steps;

import net.thucydides.core.annotations.Step;
import tsum.pages.AuthorizationPage;
import tsum.pages.HomePage;


/**
 * @project testProject
 * Created by Andrey M on 01.01.2021.
 */
public class AuthorizationSteps {

	AuthorizationPage authorization;
	HomePage home;

	@Step()
	public void openAuthorizationPage(){
		authorization.open();
	}

	@Step
	public void fillAuthorizationFields(String email, String password) {
		authorization.inputEmailField(email);
		authorization.inputPasswordField(password);
	}

	@Step
	public void confirmAuthorization() {
		authorization.submitButtonClick();
	}

	@Step
	public void shouldSeeSelfOnTheMenu(String text) {
		home.userHeaderFieldIsPresented();
		home.userHeaderFieldContainsText(text);
	}

	@Step
	public void shouldSeeAnErrorNotification(String text) {
		home.errorNotificationIsPresented();
		home.errorNotificationContainsText(text);
	}
}
