package tsum.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


/**
 * @project testProject
 * Created by Andrey M on 02.01.2021.
 */
public class HomePage extends PageObject {
	@FindBy(xpath = "*//div[@class='header__private']/a[contains(@class,'header__link')]")
	private WebElementFacade userHeaderField;

	@FindBy(xpath = "*//div[@class='notice info']")
	private WebElementFacade successNotification;

	@FindBy(xpath = "*//div[@class='notice error']")
	private WebElementFacade errorNotification;

	public void successNotificationIsPresented() {
		successNotification.shouldBeVisible();
	}

	public void successNotificationContainsText(String text) {
		successNotification.shouldContainText(text);
	}

	public void userHeaderFieldIsPresented() {
		successNotification.shouldBeVisible();
	}

	public void userHeaderFieldContainsText(String text) {
		successNotification.shouldContainText(text);
	}

	public void errorNotificationIsPresented() {
		successNotification.shouldBeVisible();
	}

	public void errorNotificationContainsText(String text) {
		successNotification.shouldContainText(text);
	}
}
