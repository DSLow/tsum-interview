# testProject (TSUM interview)

**Задание:**

Нужно для сайта tsum.ru для страниц регистрации и авторизации написать чеклист, в котором будет два позитивных и два негативных сценария на каждую из страниц (всего 8). После этого нужно автоматизировать любые два позитивных и два негативных сценария используя наш стек технологий.
Стек: java, gradle, junit, Serenity(framework).
Код нужно выложить на гитхаб или аналогичный ресурс. Чеклист предоставить в гуглдок

---

https://docs.google.com/spreadsheets/d/1ERlAfR2jN7f1POvEe5fz5AKBRvt4V9VfS7tpFTq0iSE/edit?usp=sharing

**Тэги сценариев:** _type_ = positive, negative; _component_ = authorization, registration.
